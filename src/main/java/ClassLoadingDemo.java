public class ClassLoadingDemo {

    static {
        System.out.println("\nClassLoadingDemo static initializer\n");
    }

    public static void main(String[] args) throws ClassNotFoundException {
        System.out.println("\nStarting main\n");

        System.out.println(CompileTimeConstantHolder.CONSTANT + "\n");

        MySubClass subClass = new MySubClass();
        System.out.println("\nsubClass.number() : " + subClass.number() + "\n");

        System.out.println("\nStaticMethod.doubleValue(): + " + StaticMethod.doubleValue() + "\n");

        // Class<?> missingClass = Class.forName("MissingClass");
    }
}

class CompileTimeConstantHolder {
    static final String CONSTANT = "constant value";
}

class StaticMethod {
    static double doubleValue() {
        return 5.5d;
    }
}

class MyClass implements MyInterface {

    {
        System.out.println("\nMyClass instance initializer\n");
    }

    static {
        System.out.println("\nMyClass static initializer\n");
    }

}

class MySubClass extends MyClass {
    int number() {
        return 4;
    }
}

interface MyInterface {}


