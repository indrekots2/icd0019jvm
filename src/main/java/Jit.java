import java.util.Random;
import java.util.stream.IntStream;

public class Jit {

    static int sum;
    static Random randomGen = new Random();

    public static void main(String[] args) {
        long[] iterations = new long[200];

        int[] numbers = randomNumbers();

        for (int i = 0; i < iterations.length; i++) {
            long start = System.nanoTime();

            sum(numbers);

            long end = System.nanoTime();
            iterations[i] = end - start;

            mutate(numbers);
        }

        for (int i = 0; i < iterations.length; i++) {
            System.out.println(iterations[i]);
        }
    }

    static int sum(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }

        return sum;
    }

    static int[] randomNumbers() {
        return IntStream.range(1, 30_000).map(i -> randomGen.nextInt(10)).toArray();
    }

    static void mutate(int[] array) {
        int i = randomGen.nextInt(array.length);
        array[i] = randomGen.nextInt(10);
    }
}
