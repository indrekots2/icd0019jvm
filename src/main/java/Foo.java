import org.apache.commons.lang3.RandomStringUtils;

public class Foo {
    public static void main(String[] args) {
        String random = RandomStringUtils.randomAlphanumeric(10);
        System.out.println(random);
    }
}
